FROM amazonlinux:2

CMD ["mvn", "-version"]

ENV M2_HOME /opt/maven
ENV MVN_VERSION 3.6.1 
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US
ENV LC_ALL en_US.UTF-8

ARG MAVEN_DOWNLOAD_SHA512=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
RUN yum -y update
RUN yum -y install java-1.7.0-openjdk-devel
RUN yum -y install shadow-utils
RUN yum -y install wget
RUN yum -y install tar
RUN yum -y install openssh-clients
RUN yum -y install git
RUN set -o errexit -o nounset \
	&& echo "Downloading Maven" \
	&& wget -O maven.tar.gz "https://downloads.apache.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz" \
	\
	&& echo "Checking download hash" \
	&& echo "${MAVEN_DOWNLOAD_SHA512} *maven.tar.gz" | sha512sum -c - \
	\
	&& echo "Installing Maven" \
	&& tar -xvf maven.tar.gz \
	&& rm maven.tar.gz \
	&& mv "apache-maven-3.6.3" "${M2_HOME}/" \
	&& ln -s "${M2_HOME}/bin/mvn" /usr/bin/mvn \
	\
	&& echo "Adding maven user and group" \
	&& groupadd maven \
	&& useradd -g maven maven \
	&& mkdir -p /home/maven/.m2 \
	&& chown -R maven:maven /home/maven \
	\
	&& echo "Symlinking root Maven cache to mvn Maven cache" \
	&& ln -s /home/maven/.m2 /root/.m2

# Create Maven volume
USER maven
VOLUME "/home/maven/.m2"
WORKDIR /home/maven
ENV JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.261-2.6.22.2.amzn2.0.1.x86_64/
COPY settings.xml /home/maven/.m2/settings.xml

RUN set -o errexit -o nounset \
	&& echo "Testing Maven installation" \
&& mvn --version


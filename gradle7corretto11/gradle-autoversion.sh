#!/bin/sh
SUBJECT=`git log -1 --pretty=%s`
if [ `echo $SUBJECT | grep -c "Merge "` -ge 1 ]
    then
        # SSH Config para poder enviar cambios desde el gitlab runner
        mkdir -p ~/.ssh
        eval $(ssh-agent -s)
        ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
        echo "$AUTOVERSIONER_SSH_PRIVATE_KEY" | ssh-add -

        # Configura URL a traves de SSH para evitar enviar usuario y contrasenia por HTTP
        export SSH_REPOSITORY_URL="git@gitlab.com:"${CI_PROJECT_PATH}".git"
        git remote set-url --push origin "$SSH_REPOSITORY_URL"

        # Configura email y usuario para identificar los cambios hechos por el bot de CI
        git config --global user.email "ci@cencor.com"
        git config --global user.name "Continuous Integration Bot"

        currentVersion=$(git describe --tag --abbrev=0)
        currentVersionStatus=$?
        if [ $currentVersionStatus -ne 0 ]
        then
            echo "No existe ningun TAG creado. Se tomará la versión a partir de la propiedad version del archivo gradle.properties"
            currentVersion=$(cat gradle.properties | grep 'version' | cut -d'=' -f2)
        fi

		major=$(echo $currentVersion | cut -d. -f1)
		minor=$(echo $currentVersion | cut -d. -f2)
		patch=$(echo $currentVersion | cut -d. -f3)
        if
            [ `echo $SUBJECT | grep -c "feature-"` -ge 1 ] ||
            [ `echo $SUBJECT | grep -c "minor-"` -ge 1 ]
            then
                echo 'minor change'
                minor=$((minor+1))
                patch=0
        else
            if
                [ `echo $SUBJECT | grep -c "patch-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "fix-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "hotfix-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "doc-"` -ge 1 ]
            then
                echo 'patch change'
                patch=$((patch+1))
        else
            if
                [ `echo $SUBJECT | grep -c "major-"` -ge 1 ]
            then
                echo 'major change'
                major=$((major+1))
                minor=0
                patch=0
            fi
        fi
    fi

    releasedVersion="$major.$minor.$patch"
    #Actualiza version del componente
    sed -ie s/^version=.*/version=$releasedVersion/ gradle.properties

    git add gradle.properties
    git commit -m "Tag version $releasedVersion"
    git push origin HEAD:$CI_DEFAULT_BRANCH

    git tag $releasedVersion
    git push origin $releasedVersion
    echo "Se generó la versión $releasedVersion"
fi

# CI docker image
Este repositorio cuenta con la definicion de la imagen docker que se utiliza para realizar las distintas tareas de integracion continua de CENCOR.

Estab imagen es basada en la definicion de la imagen gradle/alpine y es enriquecida con git y openssh.

Git es utilizado por el autoversionador quien genera cambios y los sube al repositorio
OpenSSH se utiliza para poder subir los cambios al repositorio sin necesidad de intercambiar usuario y contraseña

## Construccion de la imagen
Con el parametro -t indicamos que el tag o la etiqueta de la imagen que sigue la estructura de company_or_user_id/image_name:version
`docker build -t cencor/gradle:latest .`

## Correr la imagen
Este paso es util para verificar manualmente que la imagen esta correcta antes de publicarla.
`docker run -it cencor/gradle /bin/sh`

## Publicar la imagen
Antes de publicar la imagen, iniciamos sesion con una cuenta que este asociada a la organizacion cencor: `https://hub.docker.com/r/cencor/`. De no hacerlo no se podrá publicar la imagen a la organizacion CENCOR en docker hub
`docker login`

`docker push cencor/gradle:latest`

# Para automatizar agregarlo en gitlab-ci

